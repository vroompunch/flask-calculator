from flask import Flask, request, jsonify

app = Flask(__name__)


#this route is defined for the defalut route of "/". It returns the text "hellow world" to the browser
@app.route('/')
def hello_world():
    return 'Hello World!'


#this route is to handle the calculation based on the JSON that is posted to it. It reads the operands and the operation and performs the respective operation and then returns the output as a text to the service/browser that posted to it.
'''
SAMPLE JSON:
{
    "operands": [1,2,3,4,5,6,7],
    "operation": "sum"
}
'''



@app.route('/calc/', methods=['GET','POST'])
def calculate():
    if request.method == 'POST':
        content = request.get_json()
        print (content)

        print content['operands']
        operands = content['operands']
        print len(operands)

        operation = content['operation']
        print operation

        if operation == 'sum':
            sum = 0
            for i in range(len(operands)):
                sum += operands[i]
            output = sum

        elif operation == 'difference':
            difference = 0
            for i in range(len(operands)):
                difference -= operands[i]
            output = difference

        elif operation == 'product':
            product = 1
            for i in range(len(operands)):
                product *= operands[i]
            output = product

        elif operation == 'divide':
            quotient = operands[0]
            for i in range(1, len(operands)):
                if operands[i] == 0:
                    return 'DIVISION ERROR! CANNOT DIVIDE BY 0'
                quotient /= operands[i]
            output = quotient

        else:
            return 'INVALID INPUT'

        return str(output)


#this function is used to start the flask app and run the server on localhost
if __name__ == '__main__':
    app.run(debug=True)
